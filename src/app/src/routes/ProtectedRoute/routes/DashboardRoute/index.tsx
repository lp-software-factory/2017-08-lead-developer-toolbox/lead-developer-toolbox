import * as React from "react";

import {LDT} from "../../../../module";
import {lazyInject} from "../../../../config-di/di-common";
import {UINavigationService} from "../../../../module/ui/services/UINavigationService";

export class ProtectedDashboardRoute extends React.Component<undefined, undefined>
{
    @lazyInject(UINavigationService) private uiNavigation: UINavigationService;

    componentWillMount(): void {
        this.uiNavigation.push({
            displayInNavbar: true,
            title: {
                formattedMessage: {
                    id: 'routes.protected.dashboard.title',
                    defaultMessage: 'Dashboard'
                }
            }
        });
    }

    componentWillUnmount(): void {
        this.uiNavigation.pop();
    }

    render(): JSX.Element {
        return (
            <div>
                <h1>Dashboard</h1>
                <LDT.Modules.Dashboard.Components.DashboardClock/>
            </div>
        );
    }
}
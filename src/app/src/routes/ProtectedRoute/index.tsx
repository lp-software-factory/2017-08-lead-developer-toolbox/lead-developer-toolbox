import * as React from "react";

import {Route} from "react-router";

import {LDT} from "../../module";
import {lazyInject} from "../../config-di/di-common";
import {UINavigationService} from "../../module/ui/services/UINavigationService";

export class ProtectedRoute extends React.Component<undefined, undefined>
{
    @lazyInject(UINavigationService) private uiNavigation: UINavigationService;

    componentWillMount(): void {
        this.uiNavigation.push({
            displayInNavbar: false,
            title: {
                formattedMessage: {
                    id: 'routes.protected.title',
                    defaultMessage: 'Protected'
                }
            }
        });
    }

    componentWillUnmount(): void {
        this.uiNavigation.pop();
    }

    render(): JSX.Element {
        return (
            <div>
                <h1>Protected</h1>

                <Route path="/protected/dashboard" component={LDT.Routes.Protected.Dashboard.Index}/>
                <Route path="/protected/artifacts" component={LDT.Routes.Protected.Artifacts.Index}/>
            </div>
        );
    }
}
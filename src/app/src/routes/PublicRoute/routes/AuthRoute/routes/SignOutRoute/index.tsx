import * as React from "react";

import {lazyInject} from "../../../../../../config-di/di-common";
import {UINavigationService} from "../../../../../../module/ui/services/UINavigationService";

export class PublicAuthSignOutRoute extends React.Component
{
    @lazyInject(UINavigationService) private uiNavigation: UINavigationService;

    componentWillMount(): void {
        this.uiNavigation.push({
            displayInNavbar: true,
            title: {
                formattedMessage: {
                    id: 'routes.public.auth.sign-out.title',
                    defaultMessage: 'Sign Out'
                }
            }
        });
    }

    componentWillUnmount(): void {
        this.uiNavigation.pop();
    }

    render(): JSX.Element {
        return (
            <div>
                <h1>Sign Out</h1>
            </div>
        );
    }
}
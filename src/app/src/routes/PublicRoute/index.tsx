import * as React from "react";

import {lazyInject} from "../../config-di/di-common";
import {UINavigationService} from "../../module/ui/services/UINavigationService";

export class PublicRoute extends React.Component<undefined, undefined>
{
    @lazyInject(UINavigationService) private uiNavigation: UINavigationService;

    componentWillMount(): void {
        this.uiNavigation.push({
            displayInNavbar: false,
            title: {
                formattedMessage: {
                    id: 'routes.public.title',
                    defaultMessage: 'Public'
                }
            }
        });
    }

    componentWillUnmount(): void {
        this.uiNavigation.pop();
    }

    render(): JSX.Element {
        return <div>
                <h1>Public</h1>
            </div>;
    }
}
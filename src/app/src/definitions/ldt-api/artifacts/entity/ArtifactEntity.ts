export interface ArtifactEntity
{
    code: string,
    title: string,
    description: string,
    updating: number, // Demo only
}

export interface Artifact
{
    entity: ArtifactEntity,
}
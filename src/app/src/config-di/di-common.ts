import {container} from "./di-container";

import getDecorators from "inversify-inject-decorators";

import {DashboardTimeService} from "../module/dashboard/services/DashboardTimeService";
import {UINavigationService} from "../module/ui/services/UINavigationService";
import {ReactRESTAdapter} from "../module/common/services/ReactRESTAdapter";

container.bind<ReactRESTAdapter>(ReactRESTAdapter)
    .to(ReactRESTAdapter)
    .inSingletonScope();

container.bind<DashboardTimeService>(DashboardTimeService)
    .to(DashboardTimeService)
    .inSingletonScope();

container.bind<UINavigationService>(UINavigationService)
    .to(UINavigationService)
    .inSingletonScope();

let {
    lazyInject,
    lazyInjectNamed,
    lazyInjectTagged,
    lazyMultiInject
} = getDecorators(container);

export {
    container,
    lazyInject,
    lazyInjectNamed,
    lazyInjectTagged,
    lazyMultiInject
};
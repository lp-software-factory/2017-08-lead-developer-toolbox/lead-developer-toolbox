import {BehaviorSubject} from "rxjs/BehaviorSubject";

interface RepositoryEntity
{
    id: string;
}

export interface CommonRepositoryItems<T> { [id: string]: BehaviorSubject<T> }

export class CommonRepository<T extends RepositoryEntity>
{
    private _items: CommonRepositoryItems<T> = {};
    private _snapshot: { [id: string]: T } = {};

    public all$: BehaviorSubject<CommonRepositoryItems<T>> = new BehaviorSubject<CommonRepositoryItems<T>>({});

    upsert(entity: T): boolean {
        if(this._items.hasOwnProperty(entity.id)) {
            this._items[entity.id].next(entity);
            this._updateAll();

            return false;
        }else{
            this._items[entity.id] = new BehaviorSubject<T>(entity);
            this._items[entity.id].subscribe(next => {
                this._snapshot[next.id] = next;
            })
        }
    }

    upsertMany(entities: T[]): number {
        let numInserted: number = 0;

        for(let entity of entities) {
            numInserted += + this.upsert(entity);
        }

        return numInserted;
    }

    get(id: string): BehaviorSubject<T> {
        if(this._items.hasOwnProperty(id)) {
            return this._items[id];
        }else{
            throw new Error(`CommonRepository entity with ID "${id}" not found`);
        }
    }

    getAllAvailableIds(): string[] {
        let result: string[] = [];

        for(let i in this._items) {
            if(this._items.hasOwnProperty(i)) {
                result.push(i);
            }
        }

        return result;
    }

    getSnapshot(id: string): T {
        if(this._snapshot.hasOwnProperty(id)) {
            return this._snapshot[id];
        }else{
            throw new Error(`CommonRepository entity with ID "${id}" not found`);
        }
    }

    remove(id: string): boolean {
        if(this._items.hasOwnProperty(id)) {
            this._items[id].complete();
            delete this._items[id];
            this._updateAll();

            return true;
        }else{
            return false;
        }
    }

    private _updateAll(): void {
        this.all$.next(this._items);
    }
}
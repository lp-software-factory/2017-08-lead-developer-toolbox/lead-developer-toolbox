import * as React from "react";

import "./style.less";

import {Subject} from "rxjs/Subject";
import * as leftPad from "left-pad";

import {lazyInject} from "../../../../config-di/di-common";
import {DashboardTimeService} from "../../services/DashboardTimeService";

interface DashboardClockState
{
    next: {
        hours: string,
        minutes: string,
        seconds: string,
    },
}

export class DashboardClock extends React.Component<undefined, DashboardClockState>
{
    private unmount: Subject<void> = new Subject<void>();

    @lazyInject(DashboardTimeService) private service: DashboardTimeService;

    constructor() {
        super();

        this.state = {
            next: {
                hours: '00',
                minutes: '00',
                seconds: '00',
            }
        };
    }

    render(): JSX.Element {
        return (
            <div className="ldt___dashboard-clock">
                <div className="ldt___dashboard-clock_hours">{this.state.next.hours}</div>
                <div className="ldt___dashboard-clock_minutes">{this.state.next.minutes}</div>
                <div className="ldt___dashboard-clock_seconds">{this.state.next.seconds}</div>
            </div>
        );
    }

    componentDidMount(): void {
        console.log('[DEBUG] Dashboard Clock starts');

        this.service.current
            .takeUntil(this.unmount)
            .subscribe(next => {
                this.setState({
                    next: {
                        hours: leftPad('' + next.hours, 2, '0'),
                        minutes: leftPad('' + next.minutes, 2, '0'),
                        seconds: leftPad('' + next.seconds, 2, '0'),
                    },
                });
            })
        ;
    }

    componentWillUnmount(): void {
        console.log('[DEBUG] Dashboard Clock stops');

        this.unmount.next(undefined);
    }
}
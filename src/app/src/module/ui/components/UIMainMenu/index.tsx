import * as React from "react";

import {FormattedMessage} from 'react-intl';
import {Link} from "react-router-dom";

export class UIMainMenu extends React.Component<undefined, undefined>
{
    render(): JSX.Element {
        return (
            <div>
                <aside className="menu">
                    <p className="menu-label">
                        <FormattedMessage
                            id="ui.main-menu.menu.general"
                            defaultMessage="General"
                        />
                    </p>
                    <ul className="menu-list">
                        <li>
                            <Link to="/protected/dashboard">
                                <FormattedMessage
                                    id="ui.main-menu.menu.general.items.dashboard"
                                    defaultMessage="General"
                                />
                            </Link>
                        </li>
                        <li>
                            <Link to="/protected/artifacts">
                                <FormattedMessage
                                    id="ui.main-menu.menu.general.items.artifacts"
                                    defaultMessage="Artifacts"
                                />
                            </Link>
                        </li>
                    </ul>
                    <p className="menu-label">
                        <FormattedMessage
                            id="ui.main-menu.menu.account"
                            defaultMessage="Account"
                        />
                    </p>
                    <ul className="menu-list">
                        <li>
                            <Link to="/public">
                                <FormattedMessage
                                    id="ui.main-menu.menu.account.items.sign-out"
                                    defaultMessage="Sign Out"
                                />
                            </Link>
                        </li>
                    </ul>
                </aside>
            </div>
        );
    }
}
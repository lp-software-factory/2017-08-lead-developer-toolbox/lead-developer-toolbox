import * as React from "react";

import {BehaviorSubject} from "rxjs/BehaviorSubject";

import {LDT} from "../../../../module";
import {Artifact} from "../../../../definitions/ldt-api/artifacts/entity/ArtifactEntity";

export interface ArtifactsListProps
{
    items: {
        listItemId: string,
        artifact$: BehaviorSubject<Artifact>,
    }[],
}

export class ArtifactList extends React.Component<ArtifactsListProps, undefined>
{
    render(): JSX.Element {
        if(this.props && this.props.items && this.props.items.length > 0) {
            let items = this.props.items.map((item) => (
                <li key={item.listItemId}>
                    <LDT.Modules.Artifact.Components.ArtifactListItem artifact$={item.artifact$}/>
                </li>
            ));

            return (
                <div>
                    <ul>
                        {items}
                    </ul>
                </div>
            )
        }else{
            return <div>No artifacts available.</div>
        }
    }
}
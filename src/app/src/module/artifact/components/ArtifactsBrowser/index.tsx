import * as React from "react";
import {Subject} from "rxjs/Subject";

import {LDT} from "../../../../module";
import {ArtifactsListProps} from "../ArtifactList/index";
import {ArtifactFeed, factoryArtifactFeed} from "../../feed/ArtifactFeed";

interface ArtifactsBrowserState
{
    ready: boolean,
    list: ArtifactsListProps,
}

export class ArtifactsBrowser extends React.Component<undefined, ArtifactsBrowserState>
{
    private unmount: Subject<void> = new Subject<void>();

    private list: ArtifactsListProps = { items: [] };
    private ready: boolean = false;
    private feed: ArtifactFeed;

    componentDidMount(): void {
        this.feed = factoryArtifactFeed(undefined);
        this.feed.takeUntil(this.unmount);

        this.feed.view
            .takeUntil(this.unmount)
            .subscribe(next => {
                this.list = {
                    items: next.map(n => {
                        return {
                            listItemId: n.id,
                            artifact$: n.entity$,
                        }
                    }),
                };

                this.update();
            });

        this.feed.fetchAll()
            .takeUntil(this.unmount)
            .subscribe(
                undefined,
                undefined,
                () => {
                    this.ready = true;
                    this.update();
                },
            );
    }

    componentWillUnmount(): void {
        this.unmount.next(undefined);
    }

    render(): JSX.Element {
        if(this.ready && this.state) {
            return (
                <LDT.Modules.Artifact.Components.ArtifactList items={this.state.list.items}/>
            );
        }else{
            return (
                <div>Loading...</div>
            );
        }
    }

    update(): void {
        this.setState({
            ready: this.ready,
            list: this.list
        });
    }
}
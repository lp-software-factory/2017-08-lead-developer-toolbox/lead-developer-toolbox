import {Observable} from "rxjs/Observable";

import {lazyInject} from "../../../config-di/di-common";
import {Artifact} from "../../../definitions/ldt-api/artifacts/entity/ArtifactEntity";
import {ArtifactRESTService} from "../../../definitions/ldt-api/artifacts/service/ArtifactRESTService";
import {CommonRepository} from "../../common/util/CommonRepository";
import {injectable} from "inversify";
import {DI_TYPES} from "../../../config-di/di-types";

export interface ArtifactRepositoryEntity extends Artifact { id: string; }

@injectable()
export class ArtifactRepository
{
    public store: CommonRepository<ArtifactRepositoryEntity> = new CommonRepository<ArtifactRepositoryEntity>();

    @lazyInject(DI_TYPES.ArtifactRESTService) rest: ArtifactRESTService;

    fetchAll(): Observable<void> {
        let obs = Observable.create(loadAllObserver => {
            this.rest.getAll()
                .map(next => {
                    return next.artifacts.map(a => {
                        return { ...a, id: a.entity.code };
                    })
                })
                .subscribe(
                    next => { this.store.upsertMany(next) },
                    error => {
                        loadAllObserver.error(error);
                        loadAllObserver.complete();
                    },
                    () => loadAllObserver.complete(),
                );
        });

        obs.publish().connect();

        return obs;
    }
}
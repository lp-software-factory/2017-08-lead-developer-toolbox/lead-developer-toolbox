import {Observable} from "rxjs/Observable";
import {Observer} from "rxjs/Observer";
import {injectable} from "inversify";

import {ArtifactRESTServiceInterface} from "../../../definitions/ldt-api/artifacts/service/ArtifactRESTService";
import {ArtifactsGetAllResponse200} from "../../../definitions/ldt-api/artifacts/path/get-all";

@injectable()
export class ArtifactMockRESTService implements ArtifactRESTServiceInterface
{
    public static MAX_DELAY_IN_SECONDS = 3;

    getAll(): Observable<ArtifactsGetAllResponse200> {
        return Observable.create((observer: Observer<ArtifactsGetAllResponse200>) => {
            setTimeout(() => {
                observer.next({
                    success: true,
                    artifacts: require('./../fixtures/artifacts.fixture.json'),
                });

                observer.complete();
            }, Math.floor(Math.random() * 1000 * ArtifactMockRESTService.MAX_DELAY_IN_SECONDS));
        }).share();
    }
}
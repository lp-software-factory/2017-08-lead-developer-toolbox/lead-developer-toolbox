const express = require('express');
const bodyParser = require("body-parser");
const path = require('path');

const app = express();

app.use('/public', express.static(path.join(__dirname, '/../public')));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get('*', (req, res, next) => {
    res.sendFile(path.resolve(__dirname + '\\..\\app\\index.html'));
});

const server = app.listen(process.env.PORT || 3000, () => {
    console.log(`server running on port ${server.address().port}`)
});